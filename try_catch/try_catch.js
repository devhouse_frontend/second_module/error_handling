/**
 * --------- ПРИМЕРЫ -------------- *
 *       ОБРАБОТКА ОШИБОК           *
 * -------------------------------- *
 */
/*
try {
... пробуем выполнить код...
} catch(e) {
... обрабатываем ошибки ...
} finally {
... выполняем всегда ...
}
*/

/**
 * -------------------------------- *
 *      ОБЫКНОВЕННЫЙ TRY CATCH      *
 * -------------------------------- *
 */
try {
    // пробуем выполнить этот блок
    console.log('Начало блока try');
    // какие-то инструкции
    console.log('Конец блока try');
    // код без ошибок
} catch (err) {
    // в случае ошибки в блоке try
    console.log('Catch игнорируется, так как нет ошибок');
}

/**
 * -------------------------------- *
 *      TRY CATCH  С ОШИБКОЙ        *
 * -------------------------------- *
 */
try {
    // пробуем выполнить этот блок
    console.log('Начало блока try');
    jyrN // что-то некорректное, где скрипт упадет
    console.log('Конец блока try');
    // код без ошибок
} catch (err) {
    // в случае ошибки в блоке try
    console.log('Движок не может понять что написано в try');
}

/**
 * -------------------------------- *
 *          ОБЪЕКТ ОШИБКА           *
 * -------------------------------- *
 */
try {
    djddkk; // ошибка, переменная не определена!
} catch (errSome) {
    console.log(errSome.name); // ReferenceError
    console.log(errSome.message); // djddkk is not defined
}
// но использовать error необязательно
try {
    djfdsnds;
} catch { //  без err
    console.log("Что-то случилось!")
}

/**
 * -------------------------------- *
 *        TRY CATCH FINALLY         *
 * -------------------------------- *
 */
/**
* Какая-нибудь функция
* @param число
* @returns {count} сколько делятся на 3 и больше 0
*/
function doSomething(par) {
    let count = 0;
    for (let i = 1; i < par; i++) {
        if (i % 3 == 0) count++;
        console.log(i);
    }
    return count;
}
// переменная в которой будем засекать время выполнения
let diff = 0;
let start = Date.now(); // фиксируем текущее время
/**
* Выполнение функции в конструкции try catch finally
*/
try {
    // result = doSomething(djdjfjdj); // пробуем запустить
    result = doSomething(8); // пробуем запустить
} catch (e) {
    result = 0; // если ошибка - результат пуст
} finally {
    diff = Date.now() - start; // в любом случае замеряем время
}
// выводим результат
console.log('Результат - ' + result || 'Возникла ошибка!');
// выводим время
console.log(`Это заняло ${diff} миллисекунд`);